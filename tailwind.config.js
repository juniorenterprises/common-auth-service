/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

const colors = require('tailwindcss/colors')

module.exports = {
    purge: [/* setup in webpack.config.js */],
    darkMode: false, // or 'media' or 'class'
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            gray: colors.coolGray,
            primary: {
                DEFAULT: 'var(--color-primary)',
                800:'var(--color-primary-800)',
                700:'var(--color-primary-700)',
                600:'var(--color-primary-600)',
                500:'var(--color-primary-500)',
                400:'var(--color-primary-400)',
                300:'var(--color-primary-300)',
                200:'var(--color-primary-200)',
            },
            text: {
                1:'var(--color-text-1)',
                2:'var(--color-text-2)',
                3:'var(--color-text-3)',
            },
            ui: {
                1:'var(--color-ui-1)',
                2:'var(--color-ui-2)',
                3:'var(--color-ui-3)',
            },
            danger: 'var(--color-danger)',
            warning: 'var(--color-warning)',
            success: 'var(--color-success)',
        },
        extend: {
        }
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
