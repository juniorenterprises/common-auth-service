<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\Application\ApplicationRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

final class LeagueClientRepository implements ClientRepositoryInterface
{
    /**
     * @var ApplicationRepository
     */
    private $clientRepository;

    public function __construct(ApplicationRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function getClientEntity($clientIdentifier): ?ClientEntityInterface
    {
        if (!is_string($clientIdentifier)) {
            return null;
        }

        $client = $this->clientRepository->withId($clientIdentifier);
        return new LeagueClient(
            $client->id(),
            $client->getName(),
            $client->getRedirectUris(),
            $client->isConfidential()
        );
    }

    public function validateClient($clientIdentifier, $clientSecret, $grantType): bool
    {
        $client = $this->clientRepository->withId($clientIdentifier);

        if ($client === null) {
            return false;
        }

        if ($client->secret() !== $clientSecret) {
            // this may need to be adapted depending on the grant type
            // for now we only allow those that include a secret
            return false;
        }

        if (false === array_search($grantType, $client->allowedGrantTypes())) {
            return false;
        }

        return true;
    }
}
