<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\OAuth\RefreshToken;
use App\Core\OAuth\RefreshTokenRepository;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;

final class LeagueRefreshTokenRepository implements RefreshTokenRepositoryInterface
{
    /**
     * @var RefreshTokenRepository
     */
    private $tokenRepository;

    public function __construct(RefreshTokenRepository $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    public function getNewRefreshToken(): RefreshTokenEntityInterface
    {
        return new LeagueRefreshTokenEntity();
    }

    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity)
    {
        $token = new RefreshToken(
            $refreshTokenEntity->getIdentifier(),
            $refreshTokenEntity->getExpiryDateTime(),
            $refreshTokenEntity->getAccessToken()->getIdentifier()
        );
        $this->tokenRepository->save($token);
    }

    public function revokeRefreshToken($tokenId): void
    {
        $token = $this->tokenRepository->withId($tokenId);
        if ($token === null) {
            return;
        }
        $this->tokenRepository->delete($token);
    }

    public function isRefreshTokenRevoked($tokenId): bool
    {
        $token = $this->tokenRepository->withId($tokenId);
        if ($token === null) {
            return true;
        }
        return false;
    }
}
