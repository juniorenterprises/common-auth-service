<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportTrait;
use Symfony\Component\Security\Http\Authenticator\Passport\UserPassportInterface;

class ExternalUserPassport implements UserPassportInterface
{
    use PassportTrait;

    /**
     * @var UserInterface
     */
    protected $user;

    public function __construct(ExternalUserBadge $externalUserBadge)
    {
        $this->addBadge($externalUserBadge);
    }

    public function getUser(): UserInterface
    {
        if (null === $this->user) {
            if (!$this->hasBadge(ExternalUserBadge::class)) {
                throw new \LogicException('Cannot get the Security user, no username or UserBadge configured for this passport.');
            }

            $this->user = $this->getBadge(ExternalUserBadge::class)->getUser();
        }

        return $this->user;
    }
}
