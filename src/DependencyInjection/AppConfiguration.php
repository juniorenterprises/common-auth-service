<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class AppConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('app');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->arrayNode('email')
                ->children()
                ->scalarNode('from')->cannotBeEmpty()->end()
                ->scalarNode('from_name')->cannotBeEmpty()->end()
                ->end()
            ->end()
            ->scalarNode('domain')->cannotBeEmpty()->end()
            ->arrayNode('external_login')
            ->children()
                ->arrayNode('global_providers')
                ->arrayPrototype()
                    ->children()
                        ->enumNode('type')->isRequired()->values(['google', 'microsoft', 'generic'])->end()
                        ->scalarNode('name')->isRequired()->end()
                        ->scalarNode('clientId')->end()
                        ->scalarNode('clientSecret')->end()
                        ->scalarNode('authorizeEndpoint')->end()
                        ->scalarNode('tokenEndpoint')->end()
                        ->scalarNode('hostedDomain')->defaultValue('*')->end()
                        ->arrayNode('scopes')->defaultValue([])->scalarPrototype()->end()
                    ->end()
                ->end()
                ->end()
            ->end()
            ->end() // twitter
            ->end()
        ;

        return $treeBuilder;
    }
}
