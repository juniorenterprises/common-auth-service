<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\Organisation\Organisation;
use App\Core\Organisation\OrganisationRepository;
use App\Core\Shared\Domain;
use App\Core\User\User;
use App\Core\User\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserCreateCommand extends Command
{
    protected static $defaultName = 'app:user:create';

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var OrganisationRepository
     */
    private $organisationRepo;

    public function __construct(UserRepository $userRepository, OrganisationRepository $organisationRepo)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->organisationRepo = $organisationRepo;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a new user')
            ->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('given_name', InputArgument::REQUIRED)
            ->addArgument('family_name', InputArgument::REQUIRED)
            ->addArgument('encryptedPassword', InputArgument::REQUIRED)
            ->addOption('admin', 'a', InputOption::VALUE_OPTIONAL, '', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');

        $user = new User();
        $user->setEmail($email)
             ->setPassword($input->getArgument('encryptedPassword'))
             ->setGivenName($input->getArgument('given_name'))
             ->setFamilyName($input->getArgument('family_name'))
             ->setOrganisation($this->getOrCreateOrganisation($email))
             ->setCreatedAt(new \DateTimeImmutable());

        if ($input->getOption('admin')) {
            $user->setRoles(['ROLE_ADMIN']);
        }

        $this->userRepository->save($user);

        $io->success('User created!');
        return Command::SUCCESS;
    }

    private function getOrCreateOrganisation($email): Organisation
    {
        $domain = Domain::fromEmail($email);
        $organisation = $this->organisationRepo->withDomain($domain);

        if ($organisation === null) {
            $organisation = Organisation::newFromDomain($domain, 'local');
            $this->organisationRepo->persist($organisation);
        }

        if (!$organisation->supportsAuthenticationThrough(new ProviderReference(true, 'local'), $domain)) {
            throw new \RuntimeException('organisation does not accept this provider');
        }

        return $organisation;
    }
}
