<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Form;

use App\Core\Organisation\OrganisationAuthenticationProvider;
use Burgov\Bundle\KeyValueFormBundle\Form\Type\KeyValueType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganisationAuthenticationProviderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add(
                'type',
                ChoiceType::class,
                [
                      'choices' => [
                          'Global' => 'global',
                          'Google' => 'google',
                          'Microsoft' => 'microsoft'
                      ]
                  ]
            )
            ->add('domain')
            ->add(
                'configuration',
                KeyValueType::class,
                [
                    'value_type' => TextType::class,
                    'allowed_keys' => [
                        'Client-ID *' => 'clientId',
                        'Secret *' => 'clientSecret', // Would be great if this could be PWD form type
                        'Authorize Endpoint *' => 'authorizeEndpoint', // used for type generic
                        'Token Endpoint *' => 'tokenEndpoint', // used for type generic
                        'Provider (Global) *' => 'provider', // used for type global
                        'Hosted Domain (Google)' => 'hostedDomain', // used for type google
                        'Scopes' => 'scopes', // used for type generic (optional)
                    ],
                    'attr' => ['data-formcollection-label-value' => 'Option']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => OrganisationAuthenticationProvider::class,
            ]
        );
    }
}
