<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\Organisation;

use App\Core\Shared\Domain;

interface OrganisationRepository
{
    /**
     * get all organisations
     *
     * @return Organisation[]
     */
    public function all(): array;

    public function withDomain(Domain $domain): ?Organisation;

    public function withId(string $id): ?Organisation;

    public function persist(Organisation $organisation): void;

    public function providerWithId(string $providerId): ?OrganisationAuthenticationProvider;
}
