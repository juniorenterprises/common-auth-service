<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication\Exception;

use App\Core\NetworkAuthentication\NetworkAuthenticationException;

class UnknownProvider extends NetworkAuthenticationException
{
    public static function globalWithId(string $providerId)
    {
        return new self(
            sprintf(
                'Unknown global provider (%s) requested',
                $providerId,
            )
        );
    }

    public static function withId(string $providerId)
    {
        return new self(
            sprintf(
                'Unkonwn provider (%s) requested',
                $providerId,
            )
        );
    }
}
