<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

/**
 * Makes available the different clients for all configured providers
 */
interface ProviderClientService
{
    public function getClient(string $providerId): Client;

    public function getGlobalClient(string $providerKey): Client;
}
