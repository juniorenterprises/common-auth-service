<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ConnectedAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @var string
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="connectedWith")
     */
    private $user;

    /**
     * @var ProviderReference
     * @ORM\Embedded(class="App\Core\NetworkAuthentication\Domain\ProviderReference")
     */
    private $provider;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $externalId;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $externalUsername;

    public function __construct(
        User $user,
        ProviderReference $provider,
        string $externalId,
        string $externalUsername
    ) {
        $this->user = $user;
        $this->provider = $provider;
        $this->externalId = $externalId;
        $this->externalUsername = $externalUsername;
    }

    public function user(): User
    {
        return $this->user;
    }

    public function provider(): ProviderReference
    {
        return $this->provider;
    }

    public function externalId(): string
    {
        return $this->externalId;
    }

    public function externalUsername(): string
    {
        return $this->externalUsername;
    }
}
