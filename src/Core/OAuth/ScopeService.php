<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth;

use App\Core\Application\Application;
use App\Core\User\User;

class ScopeService
{
    const SCOPES = [
        // openid connect
        'openid' => ['ROLE_USER'],
        'profile' => ['ROLE_USER'],
        'email' => ['ROLE_USER'],
        // custom
        'organisation.read-only' => ['ROLE_ADMIN'],
        'user.read-only' => ['ROLE_ADMIN'],
        'user.self.read-only' => ['ROLE_USER'],
    ];

    public function isValidScope(string $scope): bool
    {
        return array_key_exists($scope, self::SCOPES);
    }

    /**
     * removes or adds to the requested scope depending
     * on what matches the client/user
     * @return string[]
     */
    public function matchScopes(
        array $scopes,
        Application $client,
        User $user = null
    ): array {
        // 1st: check what the client is allowed to do
        // TODO needs client roles
        // 1st. client needs to have scopes.

        // 2nd: check what the user is allowed to
        // needs user roles
        array_filter(
            $scopes,
            function ($scope) use ($user) {
                $requiredRoles = self::SCOPES[$scope];
                $user->hasGrantedAnyOf($requiredRoles);
            }
        );

        return $scopes;
    }
}
