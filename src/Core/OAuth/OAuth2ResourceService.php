<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth;

use Psr\Http\Message\ServerRequestInterface;

interface OAuth2ResourceService
{
    public function validateAuthenticatedRequest(ServerRequestInterface $request): ServerRequestInterface;
}
