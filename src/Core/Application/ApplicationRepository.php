<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\User\User;

interface ApplicationRepository
{
    /**
     * @return Application[]
     */
    public function all(): array;

    public function withId(string $id): ?Application;

    /**
     * @return Application[]
     */
    public function allFor(User $user): array;
}
