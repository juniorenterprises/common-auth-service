<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\Shared;

final class Domain
{
    /**
     * @var string
     */
    private $domain;

    public static function fromString(string $domainStr)
    {
        return new self($domainStr);
    }

    public static function fromEmail(string $email)
    {
        $domainStr = substr($email, strpos($email, "@") + 1);
        return new self($domainStr);
    }

    private function __construct(string $domain)
    {
        if (false === filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
            throw new \InvalidArgumentException('invalid domain');
        }
        $this->domain = $domain;
    }

    public function value(): string
    {
        return $this->domain;
    }

    public function __toString()
    {
        return $this->value();
    }
}
