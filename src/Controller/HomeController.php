<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Core\Application\ApplicationRepository;
use App\Core\User\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

final class HomeController extends AbstractController
{
    /**
     * @var ApplicationRepository
     */
    private $applicationRepository;

    public function __construct(ApplicationRepository $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new \LogicException('no other users should be available');
        }

        $applications = $this->applicationRepository->allFor($user);
        $organisation = $user->getOrganisation();

        return $this->render('home/index.html.twig', [
            'applications' => $applications,
            'user' => $user,
            'organisation' => $organisation,
        ]);
    }
}
