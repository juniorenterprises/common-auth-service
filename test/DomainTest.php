<?php
/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App;

use PHPUnit\Framework\TestCase;

class DomainTest extends TestCase
{

    public function testFromEmail()
    {
        $domain = Domain::fromEmail('test@example.com');
        $this->assertEquals($domain, "example.com");
    }
}
