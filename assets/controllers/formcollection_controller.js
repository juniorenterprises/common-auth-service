/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Controller} from 'stimulus';

export default class extends Controller {
    static targets = ["dropdown"];
    static values = {prototype: String, label: String};
    count;

    connect() {
        this.count = this.element.childElementCount;

        this.addButton = this._htmlToElement('<button data-action="formcollection#add" type="button" class="btn-primary">Add a ' + this.labelValue + '</button>')
        this.element.append(this.addButton);

        // TODO remove feature
    }

    disconnect() {
        this.element.removeChild(this.addButton);
    }

    add() {
        let index = this.count;
        let entry = this.prototypeValue;
        entry = entry.replace(/__name__label__/g, index);
        entry = entry.replace(/__name__/g, index);
        const entryElement = this._htmlToElement(entry)
        this.element.insertBefore(entryElement, this.addButton);
        this.count = this.count + 1;
    }

    _htmlToElement(html) {
        var template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }
}
