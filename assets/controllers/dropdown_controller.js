/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = [ "dropdown" ]

/*
    Profile dropdown panel, show/hide based on dropdown state.

    Entering: "transition ease-out duration-100"
    From: "transform opacity-0 scale-95"
    To: "transform opacity-100 scale-100"
    Leaving: "transition ease-in duration-75"
    From: "transform opacity-100 scale-100"
    To: "transform opacity-0 scale-95"
*/

    open(event) {
        event.stopImmediatePropagation();
        if (this.dropdownOpen) {
            return this.close();
        }
        this.dropdownOpen = true;
        this.dropdownTarget.classList.add('transition', 'ease-out', 'duration-100', 'transform', 'opacity-100', 'scale-100');
    }

    close() {
        this.dropdownOpen = false;
        this.dropdownTarget.classList.remove('transition', 'ease-out', 'duration-100', 'transform', 'opacity-100', 'scale-100');
    }
}
